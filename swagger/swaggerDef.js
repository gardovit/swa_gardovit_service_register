const swaggerDefinition = {
	openapi: "3.0.0",
	info: {
		title: "Service register",
		version: "1.0.0",
		description: "API documentatin for service register",
	},
	servers: [
		{
			url: "service_regisgter:80",
			description: "Docker compose address",
		},
	],
};

module.exports = swaggerDefinition;
