const swaggerJSDoc = require('swagger-jsdoc');
const swaggerDefinition = require('./swaggerDef');
const yaml = require('yamljs');
const fs = require('fs');
const path = require('path');

const options = {
  swaggerDefinition,
  apis: [
	'htdocs/**/*.php'
  ]
};

const swaggerSpec = swaggerJSDoc(options);

// Save the Swagger spec as a YAML file
const yamlSpec = yaml.stringify(swaggerSpec, 10, 2);
fs.writeFileSync(path.join(__dirname, '/', 'swagger.yaml'), yamlSpec, 'utf8');