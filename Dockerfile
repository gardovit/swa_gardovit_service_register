FROM php:8.3-apache
WORKDIR /var/www/html

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Mod Rewrite
RUN a2enmod rewrite

# Linux Library
RUN apt-get update -y && apt-get install -y \
    libicu-dev \
    libmariadb-dev \
    unzip zip \
    zlib1g-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    sqlite3

# Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# PHP Extension
RUN docker-php-ext-install gettext intl pdo_mysql gd mysqli

RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd


RUN pecl install redis
RUN rm -rf /tmp/pear
RUN docker-php-ext-enable redis
# Install Redis server
RUN apt-get install -y redis-server
COPY resources/redis.conf /usr/local/etc/redis/redis.conf



#Create fresh sqlite database
RUN mkdir -p /usr/local/database
RUN sqlite3 /usr/local/database/database.sqlite ""
RUN chmod -R 775 /usr/local/database
RUN chmod a+rw /usr/local/database  /usr/local/database/*
ENV DATABASE_LOCATION /usr/local/database/database.sqlite



ENV LOG_FILEPATH /var/log/service_log.txt
RUN touch /var/log/service_log.txt
RUN chmod a+rw /var/log/service_log.txt


#Copy integratino tests
COPY resources/integration_test.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/integration_test.sh

#Entry point script
COPY resources/init.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/init.sh


COPY ./htdocs/ /var/www/html/
RUN composer install


CMD ["init.sh"]
