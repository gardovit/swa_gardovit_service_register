<?php

require '../vendor/autoload.php';


use OpenApi\Annotations as OA;

//Encodes data into JSON and send it as response
function sendJsonResponse($data) {
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
	echo json_encode($data);
}

require '../utils/database.php';
require '../utils/log.php';
require '../utils/circuitBreaker.php';
require '../utils/router.php';

//Endpoints
require '../endpoints/health.php';
require '../endpoints/get.php';
require '../endpoints/register.php';
require '../endpoints/remove.php';


function sendJsonPostRequest($URL, $payload){
	$jsonData = json_encode($payload);
	$curl_handle = curl_init($URL);

	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

	curl_setopt($curl_handle, CURLOPT_POST, 1);

	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $jsonData);

	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curl_handle);
	echo var_dump($response);

	if(curl_errno($curl_handle)){
		echo 'Curl error: ' . curl_error($curl_handle);
	}

	curl_close($curl_handle);

	return $response;
}

function sendGetRequest($URL){
	$curl_handle = curl_init($URL);

	curl_setopt($curl_handle, CURLOPT_HTTPGET, true);

	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($curl_handle);

	if(curl_errno($curl_handle)){
		echo 'Curl error: ' . curl_error($curl_handle);
	}

	curl_close($curl_handle);

	return $response;
}


//Extract endpoint path from request
$uri = $_SERVER['REQUEST_URI'];
$parsed_url = parse_url($uri);
$path = isset($parsed_url['path']) ? $parsed_url['path'] : '/';


//Find right handler for endpoint
$router = new Router();
$router->addRoute('/actuators/health', '\Endpoint\HealthEndpoint', 'handleGet', 'GET');
$router->addRoute(['/services', '/services/{id}'], '\Endpoint\GetEndpoint', 'handleGet', 'GET');
$router->addRoute('/services', '\Endpoint\RegisterEndpoint', 'handlePost', 'POST');
$router->addRoute(['/services', '/services/{id}'], '\Endpoint\RemoveEndpoint', 'handleDelete', 'DELETE');

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$requestMethod = $_SERVER['REQUEST_METHOD'];

// Match the route
$match = $router->match($path, $requestMethod);

try {
	if($match){
		$data = call_user_func([$match['class'], $match['function']], $match['params']);
		sendJsonResponse($data);
	}
	else{
		//No route found
		http_response_code(404);
		$data = ['message' => 'route not found'];
		sendJsonResponse($data);
	}
}
catch (Exception $e) {
	http_response_code(500);
    echo 'Internal server error: ' . $e->getMessage();
}
