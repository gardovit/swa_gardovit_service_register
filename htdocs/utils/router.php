<?php

class Router {
    private $routes = [];

    public function addRoute($patterns, $class, $function, $method) {
        $this->routes[] = [
			'patterns' => (is_array($patterns) ? $patterns : [$patterns]),
            'class' => $class,
            'function' => $function,
            'method' => $method
        ];
    }

    public function match($path, $requestMethod) {
        foreach ($this->routes as $route) {
			foreach($route['patterns'] as $pattern){
				if ($this->matchPattern($pattern, $path, $params) && $requestMethod === $route['method']) {
					return [
						'class' => $route['class'],
						'function' => $route['function'],
						'params' => $params
					];
				}
			}
        }
        return null;
    }

    private function matchPattern($pattern, $path, &$params) {
        // Update the pattern to make parameters optional
        $pattern = preg_replace('/\{([a-zA-Z0-9_]+)\?\}/', '(?:([a-zA-Z0-9_]+))?', $pattern);
        $pattern = preg_replace('/\{([a-zA-Z0-9_]+)\}/', '(?:([a-zA-Z0-9_]+))', $pattern);
        $pattern = rtrim($pattern, '/');
		$pattern = '#^' . $pattern . '$#';

        if (preg_match($pattern, $path, $matches)) {
            $params = array_filter($matches, 	function($key){return $key != 0;}, ARRAY_FILTER_USE_KEY);
            return true;
        }
        return false;
    }
}