<?php


function init_db(){
	$db_filename = getenv('DATABASE_LOCATION');

	if(file_exists($db_filename) == false){
		throw new Exception("Databse file do not exists");
	}

	$db = new PDO('sqlite:'.$db_filename);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$db->exec("
		CREATE TABLE IF NOT EXISTS services (
		id INTEGER PRIMARY KEY,
		type TEXT,
		hostname TEXT,
		port INT,
		health_url TEXT,
		no_response_streak INT default 0,
		UNIQUE(hostname, port)
		)");

	return $db;
}