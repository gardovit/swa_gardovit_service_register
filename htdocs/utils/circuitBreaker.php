<?php


/**
 * Exception that indicates that breaker could not finish cacllback
 */
class ProblemOccuredException extends Exception{

}


class CircuitBreaker {
    private $redis;
    private $serviceName;
    private $failureThreshold;
    private $successThreshold;
    private $timeout;

    public function __construct($serviceName, $failureThreshold, $timeout, $successThreshold = 1) {
        $this->redis = new \Predis\Client(['host' => '0.0.0.0']);
        $this->serviceName = $serviceName;
        $this->failureThreshold = $failureThreshold;
        $this->timeout = $timeout;
        $this->successThreshold = $successThreshold;
    }

	/**
	 * Tries to xecute callback.
	 * Exception is counted as failure.
	 *
	 * @param function callback which contactservice
	 * @return float The result of the division.
	 * @throws ProblemOccuredException Exception during
	 */
    public function call($callback) {
        $state = $this->getState();

        switch ($state) {
            case 'OPEN':
                if (time() - $this->getLastFailureTime() > $this->timeout) {
                    $this->setState('HALF_OPEN');
                    return $this->attemptReset($callback);
                } else {
                    return $this->fallback(null);
                }

            case 'HALF_OPEN':
                return $this->attemptReset($callback);

            case 'CLOSED':
            default:
                return $this->execute($callback);
        }
    }

    private function execute($callback) {
        try {
            $result = $callback();
            $this->reset();
            return $result;
        } catch (\Exception $e) {
            $this->recordFailure();
            return $this->fallback($e);
        }
    }

    private function attemptReset($callback) {
        try {
            $result = $callback();
            $this->recordSuccess();
            if ($this->getSuccessCount() >= $this->successThreshold) {
                $this->reset();
            }
            return $result;
        } catch (\Exception $e) {
            $this->recordFailure();
            $this->setState('OPEN');
            return $this->fallback($e);
        }
    }

    private function reset() {
        $this->setState('CLOSED');
        $this->redis->del($this->serviceName . ':failures');
        $this->redis->del($this->serviceName . ':successes');
    }

    private function recordFailure() {
        $failures = $this->redis->incr($this->serviceName . ':failures');
        $this->redis->set($this->serviceName . ':lastFailureTime', time());
        if ($failures >= $this->failureThreshold) {
            $this->setState('OPEN');
        }
    }

    private function recordSuccess() {
        $this->redis->incr($this->serviceName . ':successes');
    }

    private function getState() {
        return $this->redis->get($this->serviceName . ':state') ?: 'CLOSED';
    }

    private function setState($state) {
        $this->redis->set($this->serviceName . ':state', $state);
    }

    private function getLastFailureTime() {
        return $this->redis->get($this->serviceName . ':lastFailureTime') ?: 0;
    }

    private function getSuccessCount() {
        return $this->redis->get($this->serviceName . ':successes') ?: 0;
    }

    private function fallback(?Exception $exception) {
		if($exception){
			log_event('circuit_breaker_fallback', 'Circuit breaker triggered fallback', [
				'service' => $this->serviceName,
				'message' => $exception->getMessage()
			]);
		}

       throw new ProblemOccuredException('Circuit breaker for service '.$this->serviceName.' triggers fallback');
    }
}