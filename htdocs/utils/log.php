<?php

use GuzzleHttp\Client;

/**
 * Write event into log and into standard output
 */
function log_event($event_type, $message, $metadata = array(), $send_to_logstash = false) {
	if($send_to_logstash){
		logstash_log($event_type, $message, $metadata);
	}

	$log_file = getenv('LOG_FILEPATH');

	$date_time = date('Y-m-d H:i:s');

	$log_entry = array(
		'timestamp' => $date_time,
		'type' => $event_type,
		'message' => $message,
		'metadata' => $metadata
	);

	$log_entry_json = json_encode($log_entry).PHP_EOL;
	file_put_contents($log_file, $log_entry_json, FILE_APPEND);

	$out = fopen('php://stdout', 'w');
	fputs($out, "\n\r".$log_entry_json."\n\r");
	fclose($out);
}


function logstash_log($event_type, $message, $metadata = array()){
	//Elastic log
	$logstashHost = getenv('LOGSTASH_URL') ?? 'http://logstash:5000';
	$client = new Client();
	$logData = [
		'timestamp' => date('Y-m-d H:i:s'),
		'message' => $message,
		'level' => 'log',
		'event_type' => $event_type,
		'metadata' => $metadata
	];

	try {
		$response = $client->post($logstashHost, [
			'json' => $logData
		]);
	} catch (Exception $e) {
		log_event('error', 'Cannot send elastic log', ['message' => $e->getMessage()]);
	}
}