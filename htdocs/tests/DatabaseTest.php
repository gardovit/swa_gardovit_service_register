<?php

use PHPUnit\Framework\TestCase;


require __DIR__.'/../utils/database.php';

class ServiceTest extends TestCase
{
	public function testDatabaseCreation()
	{
		try{
			$db = init_db();
			$this->assertTrue(true);
			$this->assertTrue($db != null);
		}
		catch(\Exception $e){
			$this->assertTrue(false);
		}
	}


	public function testServiceInsertion(){

		try{
			$db = init_db();

			$type = 'notification';
			$hostname = 'djdidwoi';
			$port = 8080;
			$health_url = '/health';

			$db->beginTransaction();
			$stmt = $db->prepare("INSERT INTO services (type, hostname, port, health_url) VALUES (:type, :hostname, :port, :health_url)");
			$stmt->bindParam(':type', $type);
			$stmt->bindParam(':hostname', $hostname);
			$stmt->bindParam(':port', $port);
			$stmt->bindParam(':health_url', $health_url);
			$stmt->execute();
			$db->commit();

			//retrieve data
			$stmt = $db->prepare("SELECT * FROM services WHERE type = :type");
			$stmt->bindParam(':type', $type);
			$stmt->execute();
			$services = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->assertTrue(count($services) == 1);

			$service = $services[array_key_first($services)];
			$this->assertTrue($service['type'] == $type);
			$this->assertTrue($service['hostname'] == $hostname);
			$this->assertTrue($service['port'] == $port);
			$this->assertTrue($service['health_url'] == $health_url);
		}
		catch(\Exception $e){
			$this->assertTrue(false);
		}
	}
}