<?php

namespace Endpoint;

class RegisterEndpoint{

	/**
	 * @openapi
	 * /service:
	 *   post:
	 *     summary: Register service in service register
	 *     description: stores data about service so that other services can found it
	 *     tags:
	 *       - register
	 *     responses:
	 *       400:
	 *         description: Wrong input. There are parameters missing or service of that type is not supported
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Unsupported service"
	 *       409:
	 *         description: This service with that hostname and port already exists
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Service on this port and hostname already registered"
	 *       500:
	 *         description: Fatal error happend (probably with database)
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Server-side error"
	 *       200:
	 *         description: Service was choosen without problems
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 message:
	 *                   type: string
	 *                   example: "Service registration successful"
	 */
	public static function handlePost(){

		$breaker = new \CircuitBreaker('sqlite', 3, 2, 1);

		$result = [];
		try{
			$result = $breaker->call(function(){
				$type = $_POST['type'] ?? null;
				$hostname = $_POST['hostname'] ?? null;
				$port = $_POST['port'] ?? null;
				$health_url = $_POST['health_url'] ?? null;

				// Check parameters
				if ($type == null || $hostname == null || $port == null || $health_url == null) {
					http_response_code(400);
					return ['error' => 'wrong parameters'];
				}

				//Only limited types of services can be registered
				$ALLOWED_SERVICE_NAMES = getenv('ALLOWED_SERVICE_NAMES');
				$ALLOWED_SERVICE_NAMES = explode(';', $ALLOWED_SERVICE_NAMES);
				if(in_array($type, $ALLOWED_SERVICE_NAMES) == false){
					//Unsupported service
					http_response_code(400);
					return ['error' => 'Unsupported service'];
				}

				//Insert into database
				$db = init_db();
				try{
					$db->beginTransaction();
					$stmt = $db->prepare("INSERT INTO services (type, hostname, port, health_url) VALUES (:type, :hostname, :port, :health_url)");
					$stmt->bindParam(':type', $type);
					$stmt->bindParam(':hostname', $hostname);
					$stmt->bindParam(':port', $port);
					$stmt->bindParam(':health_url', $health_url);
					$stmt->execute();
					$db->commit();
				}
				catch(\PDOException $e){
					$db->rollBack();
					if($e->getCode() == 23000){
						log_event('duplicate_registration', 'Service tries to registered for the second time', [
							'type' => $type,
							'hostname' => $hostname,
							'port' => $port,
							'health_url' => $health_url,
						], true);
						http_response_code(409);
						return ['error' => 'Service on this port and hostname already registered'];
					}
					else{
						throw $e;
					}
				}

				//Registration succeeded
				log_event('service_registration', 'New service registered', [
					'type' => $type,
					'hostname' => $hostname,
					'port' => $port,
					'health_url' => $health_url,
				], true);

				http_response_code(200);
				return ['message' => 'Service registration successful'];
			});

		}
		catch(\ProblemOccuredException $e){
			log_event('error', 'Sqlite error: '.$e->getMessage(), [], true);
			http_response_code(500);
			return ['error' => 'Server-side error  with database'];
		}

		return $result;
	}
};

