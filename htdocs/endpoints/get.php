<?php

namespace Endpoint;

class GetEndpoint{

	/**
	 * @openapi
	 * /service/{serviceId}:
	 *   get:
	 *     summary: Retrieval of service address
	 *     description: One of registered services of specified type is returned (random => client-side load balancing). If no type is specified then registered all services returned. Ttrailing / is not allowed
	 *     tags:
	 *       - register
	 *     parameters:
	 *       - in: path
	 *         name: serviceId
	 *         schema:
	 *           type: integer
	 *           example: 2
	 *         required: true
	 *         description: Even though it is in path it is OPTIONAL
	 *     responses:
	 *       500:
	 *         description: Fatal error happend
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Server-side error"
	 *       404:
	 *         description: service do not exist. Returns empty string
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: ""
	 *       200:
	 *         description: Service was choosen without problems
	 *         content:
	 *           application/json:
	 *             schema:
	 *               oneOf:
	 *                 - type: string
     *                   example: "djkkk7d:8080"
     *                 - type: array
     *                   items:
     *                     type: object
	 *                     properties:
	 *                       id:
	 *                         type: integer
	 *                         example: 1
	 *                       type:
	 *                         type: string
	 *                         example: "logic"
	 *                       hostname:
	 *                         type: string
	 *                         example: "doibcu7sd"
	 *                       port:
	 *                         type: integer
	 *                         example: 8080
	 *                       health_url:
	 *                         type: string
	 *                         example: "/health"
	 *                       no_response_streak:
	 *                         type: integer
	 *                         example: 2
	 */
	public static function handleGet($params){

		$breaker = new \CircuitBreaker('sqlite', 3, 2, 1);

		$data = [];
		try{
			$data = $breaker->call(function() use ($params){
				$db = init_db();
				if(isset($params[1])){
					$stmt = $db->prepare("SELECT * FROM services WHERE id = :id");
					$stmt->bindParam(':id', $params[1]);
					$stmt->execute();
					$serviceData = $stmt->fetch(\PDO::FETCH_ASSOC);

					if(!$serviceData){
						http_response_code(404);
						return "";
					}

					http_response_code(200);
					return $serviceData;
				}


				$type = $_GET['type'] ?? null;

				if($type == null){
					$stmt = $db->prepare('SELECT * FROM services');
					$stmt->execute();
					http_response_code(200);
					return $stmt->fetchAll(\PDO::FETCH_ASSOC);
				}
				else{
					$stmt = $db->prepare("SELECT * FROM services WHERE type = :type");
					$stmt->bindParam(':type', $type);
					$stmt->execute();

					$services = $stmt->fetchAll(\PDO::FETCH_ASSOC);
					$sersviceCount = count($services);

					if($sersviceCount != 0){
						//Choose random service
						http_response_code(200);
						$service = $services[random_int(0, $sersviceCount - 1)];
						return $service['hostname'].':'.$service['port'];
					}
					else{
						//service do not exist
						http_response_code(404);
						return "";
					}
				}
			});

		}
		catch(\ProblemOccuredException $e){
			log_event('error', 'Sqlite error: '.$e->getMessage(), [], true);
			http_response_code(500);
			return ['error' => 'Server-side error'];
		}

		return $data;
	}
};
