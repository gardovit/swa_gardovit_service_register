<?php
namespace Endpoint;

class RemoveEndpoint{

	/**
	 * @openapi
	 * /service/{serviceId}:
	 *   delete:
	 *     summary: removes service from service register
	 *     description: Delete service from database which cause other services to not use it
	 *     tags:
	 *       - register
	 *     parameters:
	 *       - in: path
	 *         name: serviceId
	 *         schema:
	 *           type: integer
	 *           example: 2
	 *         required: true
	 *         description: Even though it is in path it is OPTIONAL
	 *       - in: query
	 *         name: hostname
	 *         schema:
	 *           type: string
	 *           example: jdoidli8aa
	 *         description: Has to be used together with port
	 *       - in: query
	 *         name: port
	 *         schema:
	 *           type: integer
	 *           example: 8080
	 *         description: Has to be used together with hostname
	 *     responses:
	 *       400:
	 *         description: Wrong input. There are parameters missing or service of that type is not supported
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Unsupported service"
	 *       409:
	 *         description: This service with that hostname and port already exists
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Service on this port and hostname already registered"
	 *       500:
	 *         description: Fatal error happend (probably with database)
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Server-side error"
	 *       200:
	 *         description: Service was choosen without problems
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 message:
	 *                   type: string
	 *                   example: "Service registration successful"
	 */
	public static function handleDelete($params){

		//TODO - zapojit breaker
		$breaker = new \CircuitBreaker('sqlite', 3, 2, 1);

		try{
			$data = $breaker->call(function() use($params){

				if($params != null && isset($params[1]) != false){
					//Deleting by ID
					$db = init_db();
					$db->beginTransaction();
	
					//Get info about service
					$stmt = $db->prepare("SELECT * FROM services WHERE id = :id");
					$stmt->bindParam(':id', $params[1]);
					$stmt->execute();
					$serviceData = $stmt->fetch(\PDO::FETCH_ASSOC);
	
					//Delete from database
					$stmt = $db->prepare("DELETE FROM services WHERE id = :id");
					$stmt->bindParam(':id', $params[1]);
					$stmt->execute();
					$effectedRowCount = $stmt->rowCount();
	
					if($effectedRowCount){
						//Succesfully removed
						$db->commit();
						log_event('service_removal', 'Service removed', $serviceData, true);
						http_response_code(200);
						return ['removed' => true];
					}
					else{
						//No service found
						$db->commit();
						http_response_code(409);
						return ['removed' => false, 'message' => 'No service registered under provided hostname and port'];
					}
				}
	
				//Deleting with hostname and port
				
				// Parse the URL and extract parameters
				$urlParts = parse_url($_SERVER['REQUEST_URI']);
				parse_str($urlParts['query'], $params);
	
				$hostname = $params['hostname'] ?? null;
				$port = $params['port'] ?? null;
	
				// Check parameters
				if ($hostname == null || $port == null) {
					http_response_code(400);
					return ['error' => 'wrong parameters', 1 => $urlParts, 2 => $_SERVER['REQUEST_URI']];
				}
	
				$db = init_db();
				$db->beginTransaction();
				$stmt = $db->prepare("DELETE FROM services WHERE hostname = :hostname AND port = :port");
				$stmt->bindParam(':hostname', $hostname);
				$stmt->bindParam(':port', $port);
				$stmt->execute();
				$effectedRowCount = $stmt->rowCount();
	
				if($effectedRowCount){
					//Succesfully removed
					$db->commit();
					log_event('service_removal', 'Service removed', [
						'hostname' => $hostname,
						'port' => $port
					], true);
					http_response_code(200);
					return ['removed' => true];
				}
				else{
					//No service found
					$db->commit();
					http_response_code(409);
					return ['removed' => false, 'message' => 'No service registered under provided hostname and port'];
				}
			});
		}
		catch(\ProblemOccuredException $e){
			log_event('error', 'Sqlite error: '.$e->getMessage(), [], true);
			http_response_code(500);
			return ['error' => 'Server-side error with database'];
		}

		return $data;
	}
}