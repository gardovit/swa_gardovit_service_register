<?php

namespace Endpoint;

class HealthEndpoint{


	/**
	 * @openapi
	 * /health:
	 *   get:
	 *     summary: Health check endpoint
	 *     description: Response if service is running well. Jelikož je volaný každých 10 sekund, tak funguje jako cron. Dojde ke zkontrolování všech přihlášených microservis. Pokud nějaká MAXIMAL_NO_RESPONSE_STREAK krát neodpoví, tak ho odeberu ze seznamu.
	 *     tags:
	 *       - register
	 *     responses:
	 *       500:
	 *         description: Fatal error happend
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Server-side error  with database"
	 *       200:
	 *         description: Service is healthy
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 status:
	 *                   type: string
	 *                   example: "OK"
	 *                 news:
	 *                   type: array
	 *                   description: Array of importat events that happend. What services do not respond, etc.
	 *                   items:
	 *                     type: string
	 *                     example: "Notification started responding"
	 */
	public static function handleGet(){

		$breaker = new \CircuitBreaker('sqlite', 3, 2, 1);

		$news = [];

		try{
			$news = $breaker->call(function(){

				$db = init_db();
				$db->beginTransaction();

				$stmt = $db->prepare('SELECT * FROM services');
				$stmt->execute();
				$services = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$news = [];

				foreach($services as $service){
					$url = $service['hostname'].':'.$service['port'].$service['health_url'];

					//Init curl
					$curl = curl_init();
					curl_setopt($curl, CURLOPT_URL, $url);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

					$response = curl_exec($curl);

					// Check for errors
					if ($response === false) {
						//Server unresponsive
						$error = curl_error($curl);

						if($service['no_response_streak'] + 1 >= getenv('MAXIMAL_NO_RESPONSE_STREAK')){
							//Remove service from database, because it is not responding for too long
							$stmt = $db->prepare('DELETE FROM services WHERE id = :id');
							$stmt->bindParam(':id', $service['id']);
							$stmt->execute();

							log_event('removed_unresponsive', 'Service was removed because it is unresponsive', [
								'type' => $service['type'],
								'hostname' => $service['hostname'],
								'port' => $service['port'],
							], true);

							$news[] = $service['hostname'].':'.$service['port'].' was removed';
						}
						else{
							//Add +1 to unresponsive streak
							$stmt = $db->prepare('UPDATE services SET no_response_streak = no_response_streak + 1 WHERE id = :id');
							$stmt->bindParam(':id', $service['id']);
							$stmt->execute();

							log_event('unresponsive', 'Service is not responsing', [
								'type' => $service['type'],
								'hostname' => $service['hostname'],
								'port' => $service['port'],
								'no_response_streak' => $service['no_response_streak'] + 1
							], true);

							$news[] = $service['hostname'].':'.$service['port'].' not responding ('.($service['no_response_streak'] + 1).' time)';
						}

					} else {
						//Service responded healthy

						if($service['no_response_streak']){
							//reset no responsive streak
							$stmt = $db->prepare('UPDATE services SET no_response_streak = 0 WHERE id = :id');
							$stmt->bindParam(':id', $service['id']);
							$stmt->execute();

							log_event('become healthy', 'Service started to respond again', [
								'type' => $service['type'],
								'hostname' => $service['hostname'],
								'port' => $service['port'],
							], true);

							$news[] = $service['hostname'].':'.$service['port'].' started responding';
						}
					}
				}

				$db->commit();

				return $news;
			});

		}
		catch(\ProblemOccuredException $e){
			log_event('error', 'Sqlite error: '.$e->getMessage(), [], true);
			http_response_code(500);
			return ['error' => 'Server-side error with database'];
		}


		http_response_code(200);
		return [
			'status' => 'ok',
			'news' => $news
		];
	}
};
