#!/bin/bash

sleep 10

#Register service
register_service(){

  echo "Register service integration test"
  local register_response=$(curl -X POST -d "type=notification&hostname=abcdefghijk&port=8080&health_url=/health" http://localhost:80/services)
  local register_expected_response="{\"message\":\"Service registration successful\"}"
  if [[ "$register_response" == *"$register_expected_response"* ]]; then
      echo "Registration test pass"
    else
      echo "Registration test fail"
      echo "registration response was:"
      echo $register_response
      echo "Expected response was:"
      echo $register_expected_response
      exit 1
  fi
}



#Discover service
discover_service(){

  echo "Discover service integration test"
  local get_response=$(curl http://localhost:80/services?type=notification)
  local get_expected_response="abcdefghijk:8080"
  if [[ "$register_response" == *"$register_expected_response"* ]]; then
      echo "Discovery test pass"
    else
      echo "Discovery test fail"
      echo "Discovery response was:"
      echo $get_response
      echo "Expected response was:"
      echo $register_expected_response
      exit 1
  fi
}


register_service
discover_service
